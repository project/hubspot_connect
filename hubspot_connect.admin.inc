<?php

/**
 * @file
 * Administrative page callbacks for the googleanalytics module.
 */

/**
 * Implements hook_admin_settings() for module settings configuration.
 */
function hubspot_connect_admin_settings_form($form_state) {
  $form['account'] = [
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  ];

  $form['account']['hubspot_tracking_code'] = [
    '#title' => t('Tracking Code'),
    '#type' => 'textfield',
    '#default_value' => variable_get('hubspot_tracking_code', ''),
    '#size' => 15,
    '#maxlength' => 10,
    '#required' => TRUE,
    '#description' => t('<a href="@hubspot">The HubSpot tracking code</a> is unique to each HubSpot account and allows HubSpot to monitor your website traffic.', ['@hubspot' => 'https://knowledge.hubspot.com/reports/install-the-hubspot-tracking-code',]),
  ];

  return system_settings_form($form);
}

/**
 * Implements _form_validate().
 */
function hubspot_connect_admin_settings_form_validate($form, &$form_state) {
  // Trim some text values.
  $form_state['values']['hubspot_tracking_code'] = trim($form_state['values']['hubspot_tracking_code']);

  if (!preg_match('/^\d+$/', $form_state['values']['hubspot_tracking_code'])) {
    form_set_error('hubspot_tracking_code', t('A valid HubSpot tracking code contains only digits and formatted like xxxxxxx.'));
  }

}
