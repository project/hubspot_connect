
Module: Hubspot Connect
Author: Vladimir Roudakov <http://drupal.org/user/673120>


Description
===========
Adds the Hubspot tracking system to your website.

Requirements
============

* Hubspot tracking code: https://knowledge.hubspot.com/reports/install-the-hubspot-tracking-code

Installation
============
Copy the 'hubspot_connect' module directory in to your Drupal
sites/all/modules directory as usual.

Usage
=====
In the settings page enter your Hubspot tracking id.

All pages will now have the required JavaScript added to the
HTML footer can confirm this by viewing the page source from
your browser.
